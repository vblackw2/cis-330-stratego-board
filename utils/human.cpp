#include <stdlib.h>
#include <iostream>
#include <string>

#include "human.hpp"

// Get the type of the piece from a char
ElementType Human::type_from_string(std::string s) {
    switch (s[0]) {
    case '1':
        return ElementType::spy;
    case '2':
        return ElementType::scout;
    case '3':
        return ElementType::miner;
    case '9':
        return ElementType::nine;
    case 'A':
        return ElementType::ten;
    case 'B':
        return ElementType::bomb;
    case 'F':
        return ElementType::flag;
    default:
        return ElementType::unknown;
    }
}

// Prints piece placement instructions
void Human::print_placement_instructions() {
    std::string team, placement;
    if (board->get_team() == Team::red) {
        team = "Red";
        placement = "top";
    } else {
        team = "Blue";
        placement = "bottom";
    }
    std::cout << "Set up phase:\
        \n\nYou are playing as " + team +
        ", which means pieces must be placed\
        \nin the " + placement + " three rows.\
        \n\nSelect pieces to place; you may have 10x pieces total,\
        \n1x {9, A, F}, and 2x {1, 2, 3, B}\n" << std::endl;
}

// Function used to set up the pieces
void Human::setup_pieces() {
    int pieces = 0; // Total pieces placed so far
    bool invalid = false; // If previous input was invalid

    // Input loop
    while (pieces < 10) {
        std::cout << std::string(100, '\n'); // Clear screen
        if (invalid) {
            invalid = false;
            std::cerr << "INVALID INPUT!\n" << std::endl;
        }
        print_placement_instructions();
        board->print_state();

        // Get user piece input
        std::cout << "Select a piece {1, 2, 3, 9, A, B, F}\
                    \n(" << (10 - pieces) << " remaining): ";
        std::string selected;
        std::cin >> selected;
        for (char &c: selected) c = toupper(c);

        // Parse the piece
        ElementType piece = type_from_string(selected);
        if (piece == ElementType::unknown) {
            invalid = true;
            continue;
        }

        // Get x/y coordinates
        std::string xStr, yStr;
        std::cout << "X: ";
        std::cin >> xStr;
        std::cout << "Y: ";
        std::cin >> yStr;
        try {
            int x = std::stoi(xStr);
            int y = std::stoi(yStr);
            if (board->place(piece, x, y)) ++pieces; else invalid = true;
        } catch(const std::exception& e) {
            invalid = true;
        }
    }
}

// Quick setup
void Human::setup_pieces(bool quick) {
    if (!quick) return setup_pieces();
    if (board->get_team() == Team::red) {
        board->place(ElementType::flag, 0, 0);
        board->place(ElementType::bomb, 0, 1);
        board->place(ElementType::bomb, 1, 0);
        board->place(ElementType::scout, 4, 1);
        board->place(ElementType::scout, 5, 1);
        board->place(ElementType::miner, 1, 1);
        board->place(ElementType::miner, 8, 0);
        board->place(ElementType::spy, 9, 0);
        board->place(ElementType::nine, 8, 1);
        board->place(ElementType::ten, 9, 1);
    } else {
        board->place(ElementType::flag, 5, 8);
        board->place(ElementType::bomb, 9, 8);
        board->place(ElementType::bomb, 8, 9);
        board->place(ElementType::scout, 4, 8);
        board->place(ElementType::scout, 9, 9);
        board->place(ElementType::miner, 8, 8);
        board->place(ElementType::miner, 1, 9);
        board->place(ElementType::spy, 0, 9);
        board->place(ElementType::nine, 1, 8);
        board->place(ElementType::ten, 0, 8);
    }
}

// Function used to start a turn.
// The turn ends when the function resolves.
void Human::start_turn() {
    bool notMoved = true; // If the player hasn't successfully moved
    bool invalid = false; // If the player made an invalid move
    while (notMoved) {
        if (invalid) {
            invalid = false;
            std::cerr << "INVALID INPUT!\n" << std::endl;
        }
        std::cout << "Select the x/y coordinates of the piece you want to move,\
        \nthen the direction to move the piece, and how many spaces to move it.\n" << std::endl;

        // Get x/y coordinates
        int x, y;
        std::string xStr, yStr;
        std::cout << "X: ";
        std::cin >> xStr;
        std::cout << "Y: ";
        std::cin >> yStr;
        try {
            x = std::stoi(xStr);
            y = std::stoi(yStr);
        } catch(const std::exception& e) {
            invalid = true;
            continue;
        }

        // Get user direction input
        std::cout << "Select a direction {N, S, W, E}: ";
        std::string selected;
        std::cin >> selected;
        char d = selected[0];
        d = toupper(d);

        // Parse the direction
        Direction dir = d == 'N' ? Direction::up
            : d == 'S' ? Direction::down
            : d == 'W' ? Direction::left
            : d == 'E' ? Direction::right
            : Direction::unknown;
        if (dir == Direction::unknown) {
            invalid = true;
            continue;
        }

        // Get the number of spaces
        int spaces;
        std::string spaceInput;
        std::cout << "Spaces: ";
        std::cin >> spaceInput;
        try {
            spaces = std::stoi(spaceInput);
        } catch(const std::exception& e) {
            invalid = true;
            continue;
        }

        // Try moving the piece
        Report report = board->move(x, y, dir, spaces);
        if (report.result == Result::failed) {
            invalid = true;
            continue;
        }

        // Successfully moved a piece
        notMoved = false;
    }
    return;
}