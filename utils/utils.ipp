#pragma once

#include "utils.hpp"

// Matrix allocation function
template <typename T>
T ** matrix_utils::allocate_matrix(int rows, int cols, T init) {
    T ** matrix = new T*[rows];
    for (int i = 0; i < rows; ++i) {
        *(matrix + i) = new T[cols];
        for (int j = 0; j < cols; ++j) {
            *(*(matrix + i) + j) = init;
        }
    }
    return matrix;
}

// Matrix free function
template <typename T>
void matrix_utils::free_matrix(int rows, T ** matrix) {
    for (int i = 0; i < rows; ++i) {
        delete[] *(matrix + i);
    }
    delete[] matrix;
}