#pragma once

#include "../board/board.hpp"

// Abstract class for a Player using the board
// AIs should derive from this class
class Player {
    protected:

    // Reference to player specific board object
    TeamBoard * board;

    virtual void place_pieces(){};

    public:

    // Constructor
    Player(TeamBoard *teamBoard): board(teamBoard) {};

    // Function used to set up the pieces
    void setup_pieces(){};

    // Function used to start a Player's turn.
    // The turn ends when the function resolves.
    void start_turn(){};

    // Virtual destructor
    virtual ~Player() = default;
};