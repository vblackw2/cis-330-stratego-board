#include <stdlib.h>
#include <iostream>
#include <string>

#include "board.hpp"

// Base board state update function
Element * Board::state(Team team) {
    Element *state = new Element[20];

    // Get the correct sets of pieces
    Element *myPieces;
    Element *theirPieces;
    if (team == Team::red) {
        myPieces = red_pieces;
        theirPieces = blue_pieces;
    } else {
        myPieces = blue_pieces;
        theirPieces = red_pieces;
    }
    
    // Copy my pieces
    for (int i = 0; i < 10; ++i) {
        Element piece = *(myPieces + i);
        *(state + i) = piece;
    }
    // Copy enemy pieces
    for (int i = 0; i < 10; ++i) {
        Element piece = *(theirPieces + i);
        // If the piece is alive, it should be hidden from us
        if (piece.alive) piece.type = ElementType::unknown;
        *(state + 10 + i) = piece;
    }

    return state;
}

// Returns a COO representation of the board state for a specific team
int * ** Board::spmr(Team team){
    if((red_piece_count[0] || blue_piece_count[0]) == 0) throw std::runtime_error("Invalid Access");

    bool assert;
    try{
        (team == Team::blue) ? assert = true : assert = false;
    }
    catch(...){
        std::cout << "Why are you calling it?" << std::endl;
    }
    
    for (int i = 0; i < 10; ++i) {        
        for (int j = 0; j < 10; ++j) {
            if (board[i][j]->type == ElementType::deadzone || board[i][j]->type == ElementType::empty) continue;
            if (assert){
                if (board[i][j]->team == Team::red){
                    *spm[0][j] = i;
                    *spm[1][j] = j;
                    *spm[3][j] = static_cast<int>(ElementType::unknown);
                }
            }
            else{
                if (board[i][j]->team == Team::blue){
                    *spm[0][j] = i;
                    *spm[1][j] = j;
                    *spm[3][j] = static_cast<int>(ElementType::unknown);
                }
            }
            
        }
    }
    return spm;
}