#include <stdlib.h>
#include <iostream>
#include <string>

#include "board.hpp"

Result Board::check_rank(Element *myPiece, Element *theirPiece) {
    // Check for ties
    if (myPiece->type == theirPiece->type) {
        return Result::tied;
    }

    // Flag rule: success!
    if (theirPiece->type == ElementType::flag) {
        return Result::success;
    }

    // Bomb rule: automatic destruction https://stratego.fandom.com/wiki/Bombs
    if (theirPiece->type == ElementType::bomb) {
        // ...unless your piece is the miner
        return (myPiece->type == ElementType::miner) ? Result::success : Result::died;
    }

    // Ten/Spy rule: Tens beat everything except for bombs and itself
    if (theirPiece->type == ElementType::ten) {
        // Spies only win when attacking the Ten https://stratego.fandom.com/wiki/Spy
        return (myPiece->type == ElementType::spy) ? Result::success : Result::died;
    }

    // Default case: compare strengths
    int myStrength = static_cast<int>(myPiece->type);
    int theirStrength = static_cast<int>(theirPiece->type);
    return (myStrength > theirStrength) ? Result::success
        : (myStrength < theirStrength) ? Result::died
        : Result::tied;
}

// Check if a path is clear, excluding the start and final spaces
bool Board::check_path_clear(int x1, int y1, int x2, int y2) {
    if (x1 != x2) { // If the x's are not the same, we are moving along x axis
        int xStart, xEnd;
        if (x1 < x2) { // Choose the smaller of the two to start
            xStart = x1;
            xEnd = x2;
        } else {
            xStart = x2;
            xEnd = x1;
        }
        for (int x = xStart + 1; x < xEnd; ++x) { // Loop through the row, exclude first/final
            Element *e = *(*(board + y1) + x);
            if (e->type != ElementType::empty) return false;
        }
    } else { // The only other valid case is moving along y axis
        int yStart, yEnd;
        if (y1 < y2) { // Choose the smaller of the two to start
            yStart = y1;
            yEnd = y2;
        } else {
            yStart = y2;
            yEnd = y1;
        }
        for (int y = yStart + 1; y < yEnd; ++y) { // Loop through column, exclude first/final
            Element *e = *(*(board + y) + x1);
            if (e->type != ElementType::empty) return false;
        }
    }
    return true;
}

// Basic move function
Report Board::move(Team team, int pieceX, int pieceY, Direction dir, int spaces) {
    // Create a new report
    Report report = Report();

    // Exit if placements are not complete
    // if (*red_piece_count < 10 || *blue_piece_count < 10) return report;

    // Exit if team already went
    if (previous == team) return report;

    // Check if piece x/y valid
    if (pieceX < 0 || pieceX > 9 || pieceY < 0 || pieceY > 9) return report;

    // Get the acutal piece reference
    Element *piece = *(*(board + pieceY) + pieceX);

    // Check if piece to be moved is on the right team
    if (piece->team != team) return report;

    // Temporary x and y for the next position after the movement
    int newX = pieceX;
    int newY = pieceY;

    // Correctly set new x and y according to spaces
    switch (dir) {
        case Direction::up:
            newY -= spaces;
            break;
        case Direction::down:
            newY += spaces;
            break;
        case Direction::left:
            newX -= spaces;
            break;
        case Direction::right:
            newX += spaces;
            break;
        default:
            break;
    }

    // Check if colliding with board edge
    if (newX < 0 || newX > 9 || newY < 0 || newY > 9) {
        report.encountered = ElementType::deadzone;
        return report;
    }

    // Check if moving valid amount of spaces
    if ((spaces > 1 && piece->type != ElementType::scout)
        || piece->type == ElementType::bomb
        || piece->type == ElementType::flag) {
        return report;
    }

    // When moving multiple spaces, check if every space along the way is empty
    if (spaces > 1 && !check_path_clear(pieceX, pieceY, newX, newY)) {
        return report;
    }

    // Check if colliding with friendly pieces or a deadzone
    Element *newElem = *(*(board + newY) + newX);
    if (newElem->team == piece->team || newElem->type == ElementType::deadzone) {
        return report;
    }

    // Movement Logic
    Result result = Result::success;
    if (newElem->type == ElementType::empty) { // Movement to empty space
        // Swap new space with piece pointer
        *(*(board + newY) + newX) = piece;
        // Swap old space with empty space
        *(*(board + pieceY) + pieceX) = empty;
    } else {
        result = check_rank(piece, newElem);
        if (result == Result::died) {
            // Swap old space with empty
            *(*(board + pieceY) + pieceX) = empty;
            // Set piece to dead
            piece->alive = false;
            // Decrement mobile piece counters
            if (piece->team == Team::red) {
                --red_mobile;
            } else {
                --blue_mobile;
            }
        } else if (result == Result::tied) {
            // Swap both spaces to empty
            *(*(board + newY) + newX) = empty;
            *(*(board + pieceY) + pieceX) = empty;
            // Set pieces to dead
            piece->alive = false;
            newElem->alive = false;
            // Decrement mobile piece counters
            --red_mobile;
            --blue_mobile;
        } else if (result == Result::success) {
            // Swap new space with piece pointer
            *(*(board + newY) + newX) = piece;
            // Swap old space with empty space
            *(*(board + pieceY) + pieceX) = empty;
            // Update piece x/y coordinates
            piece->x = newX;
            piece->y = newY;
            // Set other piece to dead
            newElem->alive = false;
            // Decrement mobile piece counters
            if (newElem->type != ElementType::bomb) {
                if (piece->team == Team::red) {
                    --blue_mobile;
                } else {
                    --red_mobile;
                }
            }
            // Check for flag capture seperately
            if (newElem->type == ElementType::flag) {
                flag_captured = (piece->team == Team::red) ? Team::blue : Team::red;
            }
        }
    }

    // Populate report
    previous = team;
    report.result = result;
    report.encountered = newElem->type;
    return report;
}