CIS 330 Stratego AI Project Board
================================

This repository contains the C++ code for the board to be used by the AI in a game of Stratego. The board has the standard layout, but only supports the Duel Variant pieces and setup conditions.

The AI will be directly imported by the board during the game; for testing and developing AI, please fork this repository.

### Compiling
Assuming `make` is installed, simply run the command in this directory.
To clean up intermediate files, run `make clean`